package com.creations.date.Cards;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.creations.date.R;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.facebook.ads.InterstitialAd;

import java.util.List;


public class cardAdapter extends ArrayAdapter<cardObject>{

    Context context;

    private AdView adView;

    private final String TAG = cardAdapter.class.getSimpleName();
    private InterstitialAd interstitialAd;

    public cardAdapter(Context context, int resourceId, List<cardObject> items){
        super(context, resourceId, items);
    }
    public View getView(int position, View convertView, ViewGroup parent){
        cardObject card_item = getItem(position);

        if (convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_card, parent, false);
        }

        TextView name = convertView.findViewById(R.id.name);
        TextView location = convertView.findViewById(R.id.location);
        TextView school = convertView.findViewById(R.id.school);
        ImageView image = convertView.findViewById(R.id.image);

        name.setText(card_item.getName() + ", " + card_item.getAge());
        location.setText("From "+card_item.getLocation());
        school.setText(card_item.getSchool());

        if(!card_item.getProfileImageUrl().equals("default"))
            Glide.with(convertView.getContext()).load(card_item.getProfileImageUrl()).into(image);

        return convertView;

    }


}
