package com.creations.date.Login;
import android.content.Intent;
import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;


import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.creations.date.MyLocationUsingLocationAPI;
import com.creations.date.R;

import java.util.HashMap;
import java.util.Map;

import co.ceryle.radiorealbutton.RadioRealButtonGroup;


public class Gender extends AppCompatActivity {

    private Button mConfirmGender;
    private RadioRealButtonGroup mRadioGroup;

    private FirebaseAuth mAuth;
    private DatabaseReference mUserDatabase;

    private String  userId,userSex;


    private AdView adView;

    private final String TAG = Gender.class.getSimpleName();
    private InterstitialAd interstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gender);

        adView = new AdView(this, getString(R.string.banner),
                AdSize.RECTANGLE_HEIGHT_250);
        LinearLayout adContainer =  findViewById(R.id.banner_container);
        adContainer.addView(adView);
        adView.loadAd();


        interstitialAd = new InterstitialAd(this, getString(R.string.interstitial));
        interstitialAd.setAdListener(new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                Log.e(TAG, "Interstitial ad displayed.");
            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                Log.e(TAG, "Interstitial ad dismissed.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                Log.e(TAG, "Interstitial ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                Log.d(TAG, "Interstitial ad is loaded and ready to be displayed!");
                interstitialAd.show();
            }

            @Override
            public void onAdClicked(Ad ad) {
                Log.d(TAG, "Interstitial ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                Log.d(TAG, "Interstitial ad impression logged!");
            }
        });
        interstitialAd.loadAd();



        mRadioGroup = findViewById(R.id.radioRealButtonGroup);
        mRadioGroup.setPosition(0);
        mConfirmGender = findViewById(R.id.confirmGender);



        mAuth = FirebaseAuth.getInstance();
        userId = mAuth.getCurrentUser().getUid();

        mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(userId);


        mConfirmGender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                saveUserInformation();

                Intent intent = new Intent(Gender.this,MyLocationUsingLocationAPI.class);
                startActivity(intent);
                finish();
                return;
            }
        });

    }


    private void saveUserInformation() {
        final String accountType;
        int selectId = mRadioGroup.getPosition();
        switch (selectId) {
            case 1:
                accountType = "Female";
                break;
            default:
                accountType = "Male";
        }

        Map userInfo = new HashMap<>();

        userInfo.put("sex", accountType);

        switch (accountType) {
            case "Male":
                userInfo.put("interest", "Female");
                break;
            case "Female":
                userInfo.put("interest", "Male");
                break;
        }
        mUserDatabase.updateChildren(userInfo);
    }


    @Override
    protected void onDestroy() {
        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }




}
