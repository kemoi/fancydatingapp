package com.creations.date.Login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import com.creations.date.MainActivity;
import com.creations.date.R;

import java.util.HashMap;
import java.util.Map;

public class About extends AppCompatActivity {

    TextView mAbout;


    private FirebaseAuth mAuth;
    private DatabaseReference mUserDatabase;

    private String  userId, about;
    Button continueButton;

    private AdView adView;

    private final String TAG = About.class.getSimpleName();
    private InterstitialAd interstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        adView = new AdView(this, getString(R.string.banner),
                AdSize.RECTANGLE_HEIGHT_250);
        LinearLayout adContainer =  findViewById(R.id.banner_container);
        adContainer.addView(adView);
        adView.loadAd();


        interstitialAd = new InterstitialAd(this, getString(R.string.interstitial));
        interstitialAd.setAdListener(new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                Log.e(TAG, "Interstitial ad displayed.");
            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                Log.e(TAG, "Interstitial ad dismissed.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                Log.e(TAG, "Interstitial ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                Log.d(TAG, "Interstitial ad is loaded and ready to be displayed!");
                interstitialAd.show();
            }

            @Override
            public void onAdClicked(Ad ad) {
                Log.d(TAG, "Interstitial ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                Log.d(TAG, "Interstitial ad impression logged!");
            }
        });
        interstitialAd.loadAd();


        mAbout = findViewById(R.id.userAbout);
        continueButton = findViewById(R.id.continueBtn);

        mAuth = FirebaseAuth.getInstance();
        userId = mAuth.getCurrentUser().getUid();


        mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(userId);

        about = mAbout.getText().toString();

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                    saveUserInformation();
                    Intent intent = new Intent(About.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                    return;



            }
        });



        // Use TextWatcher to change button state by EditText text length.
        mAbout.addTextChangedListener(new TextWatcher() {

            // Before EditText text change.
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            // This method is invoked after user input text in EditText.
            @Override
            public void afterTextChanged(Editable editable) {
                processButtonByTextLength();
            }
        });


        // Listen to EditText key event to change button state and text accordingly.
        mAbout.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {

                // Get key action, up or down.
                int action = keyEvent.getAction();

                // Only process key up action, otherwise this listener will be triggered twice because of key down action.
                if(action == KeyEvent.ACTION_UP)
                {
                    processButtonByTextLength();
                }
                return false;
            }
        });
    }

    private void saveUserInformation() {

        about = mAbout.getText().toString();
        Map userInfo = new HashMap<>();
        userInfo.put("about", about);
        mUserDatabase.updateChildren(userInfo);

    }

    private void processButtonByTextLength()
    {
        String inputText = mAbout.getText().toString();
        if(inputText.length() > 4)
        {

            continueButton.setEnabled(true);
        }else
        {

            continueButton.setEnabled(false);
        }
    }

    private void showMessage(String message) {
        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
    }


    @Override
    protected void onDestroy() {
        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }


}
