package com.creations.date.Login;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.google.firebase.auth.FirebaseAuth;

import com.creations.date.MainActivity;
import com.creations.date.R;

public class ChooseLoginRegistrationActivity extends AppCompatActivity   {


    private  static final String TAG = "FACELOG";
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAuth = FirebaseAuth.getInstance();
        if(mAuth.getCurrentUser() != null) {
            Intent intent = new Intent(ChooseLoginRegistrationActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
            return;
        }

        requestWindowFeature(1);
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP){
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        setContentView(R.layout.activity_choose_login_registration);



        Button mFacebook = findViewById(R.id.facebookLogin);
        Button mPhone = findViewById(R.id.phoneAuthentication);
        Button muserLogin = findViewById(R.id.userLogin);
        Button mMail = findViewById(R.id.mailRegister);




        mPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChooseLoginRegistrationActivity.this, PhoneAuthentication.class);
                startActivity(intent);
                finish();
                return;
            }
        });


        mFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChooseLoginRegistrationActivity.this, FacebookActivity.class);
                startActivity(intent);
                finish();
                return;
            }
        });

        muserLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChooseLoginRegistrationActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                return;
            }
        });



        mMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChooseLoginRegistrationActivity.this, RegisterActivity.class);
                startActivity(intent);
                finish();
                return;
            }
        });



    }



}
