package com.creations.date.Login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.creations.date.SettingsActivity;
import com.facebook.ads.*;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import com.creations.date.MainActivity;
import com.creations.date.R;

import java.util.HashMap;
import java.util.Map;


public class School extends AppCompatActivity{

    private FirebaseAuth mAuth;
    private DatabaseReference mUserDatabase;

    private String  userId, schoolName;
    TextView tv;


    private AdView adView;

    private final String TAG = SettingsActivity.class.getSimpleName();
    private InterstitialAd interstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school);


        AdView adView = new AdView(this, getString(R.string.banner),
                AdSize.RECTANGLE_HEIGHT_250);
        LinearLayout adContainer =  findViewById(R.id.banner_container);
        adContainer.addView(adView);
        adView.loadAd();


        interstitialAd = new InterstitialAd(this, getString(R.string.interstitial));
        interstitialAd.setAdListener(new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                Log.e(TAG, "Interstitial ad displayed.");
            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                Log.e(TAG, "Interstitial ad dismissed.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                Log.e(TAG, "Interstitial ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                Log.d(TAG, "Interstitial ad is loaded and ready to be displayed!");
                interstitialAd.show();
            }

            @Override
            public void onAdClicked(Ad ad) {
                Log.d(TAG, "Interstitial ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                Log.d(TAG, "Interstitial ad impression logged!");
            }
        });
        interstitialAd.loadAd();

        mAuth = FirebaseAuth.getInstance();
        userId = mAuth.getCurrentUser().getUid();

        mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(userId);


        Spinner spinner =  findViewById(R.id.spinner);
        tv =  findViewById(R.id.school);
        Button confirmSchool = findViewById(R.id.confirmSchool);
        final String school;


        String[] plants = new String[]{
                "University of Nairobi",
                "Kenyatta University",
                "Moi University",
                "Egerton University",
                "Jomo Kenyatta University",
                "Maseno University",
                "Masinde Muliro University",
                "Pwani University",
                "Dedan Kimathi University of Technology",
                "Technical University of Mombasa",
                "Chuka University",
                "Kisii University",
                "Maasai Mara University",
                "Meru University of Science and Technology",
                "University of Eldoret",
        };


        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this,android.R.layout.simple_spinner_item,plants
        );


        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tv.setText(parent.getItemAtPosition(position).toString());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //Another interface callback
            }
        });


        confirmSchool.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    saveUserInformation();

                    Intent intent = new Intent(School.this,About.class);
                    startActivity(intent);
                    finish();
                    return;


            }
        });



    }


    private void saveUserInformation() {

        schoolName = tv.getText().toString();
        Map userInfo = new HashMap<>();
        userInfo.put("school", schoolName);
        mUserDatabase.updateChildren(userInfo);
    }

    private void showMessage(String message) {
        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
    }


    @Override
    protected void onDestroy() {
        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }



}